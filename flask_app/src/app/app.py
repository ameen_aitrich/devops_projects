from flask import Flask, Response, request
from mongoengine import *
import os
import json
connect(host='mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] +
        '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE'])
app = Flask(__name__)


class Name(Document):
    name = StringField()


@app.route('/')
def hello_world():
    return Response('Hello, World!', status=200, mimetype='Application/json')


@app.route('/names')
def get_names():
    [print(name.name) for name in Name.objects]
    try:
        return Response(json.dumps([name.name for name in Name.objects]) if Name.objects else 'No data', status=200, mimetype='Application/json')
    except Exception as e:
        return Response(str(e), status=500, mimetype='Application/json')


@app.route('/name')
def save_name():
    name = request.args.get('name')
    print(name)
    name_obj = Name()
    name_obj.name = name
    try:
        name_obj.save()
    except Exception as e:
        print(str(e))
    return Response(str(name_obj.save().name) + ' is Saved', status=200, mimetype='Application/json')


@app.route('/clear')
def clear():
    Name.drop_collection()
    return Response('All cleared', status=200, mimetype='Application/json')


if __name__ == "__main__":
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", True)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    app.run(host='0.0.0.0', port=ENVIRONMENT_PORT,
            debug=ENVIRONMENT_DEBUG)
