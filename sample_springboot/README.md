# Springboot-Docker

This repository has a simple Springboot app inside a docker container using docker-compose.

## Installation

Clone the repository using the clone URL and you can see the stucture as below.

    ├── src                      # Source directory for all project files
    ├── infra                    # Contains every infrastructural files.
    └── README.md

Go inside infra directory

```bash
cd infra/docker
```

```bash
docker-compose up -d
```

## Usage

You can test the app using curl or from browser directly using below URL,

```
http://ip_address:8000/test
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
