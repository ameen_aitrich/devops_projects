# Flask-MongoDB-Nginx-Docker

This repository has a simple Python Flask app using mongoDB and serving via Nginx inside a docker container using docker-compose.

## Installation

Clone the repository using the clone URL and you can see the stucture as below.


    ├── src                      # Source directory for all project files
    ├── infra                    # Contains every infrastructural files.
    └── README.md

Go inside infra directory 

```bash
cd infra/docker
```
```bash
docker-compose up -d
```
After its done, we need to configure mongoDB user credentials and database.

```bash
docker ps -a
```
Now you will see following containers running.

```bash
CONTAINER ID        IMAGE                          COMMAND                  CREATED             STATUS              PORTS                                      NAMES
6cef5ec1aa69        aitrich.com/webserver:latest   "nginx -g 'daemon of…"   16 minutes ago      Up 16 minutes       0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   webserver
bd58523a681a        aitrich.com/flask-python:3.6   "gunicorn -w 4 --bin…"   16 minutes ago      Up 16 minutes       5000/tcp                                   flask
4fa001f6e8bb        mongo:4.0.8                    "docker-entrypoint.s…"   16 minutes ago      Up 16 minutes       27017/tcp                                  mongodb
```
Out of this you will see a container named "mongodb" which is the container running mongoDB instance inside docker. First we need to enter inside the container and then we need to log into mongo and create user and database as follows.

```bash
docker exec -it mongodb bash
```
Now you can see something like this,
```bash
root@4fa001f6e8bb:/#
```
Which means you are inside the mongodb container, now we need to log into mongo and create user and database

```bash
mongo -u mongodbuser -p mongopassword123
```
The above username and password must be same as mentioned is the docker-compose.yml file. In this case it is as above.

After Login to mongo you can see somthing like this,
```js
mongodb>
```
MongoDB allows you to switch to a database that does not exist using the use database command. It creates a database when a document is saved to a collection. Therefore the database is not created here; that will happen when you save your data in the database from the API. Execute the use command to switch to the flaskdb database:

```js
mongodb>use flaskdb
```
Next, create a new user that will be allowed to access this database:

```js
mongodb>db.createUser({user: 'flaskuser', pwd: 'flaskpassword123', roles: [{role: 'readWrite', db: 'flaskdb'}]})
```
The above username and password must be same as mentioned is the docker-compose.yml file. In this case it is as above.

And thats it, You have successfully created an entire webapplication just that easy.
## Usage
You can test the app using curl or from browser directly using below URL,
```
http://ip_address/test
```
You can insert and retrieve data to database and list everything as mentioned below,

```
http://ip_address/name?name=<yourname> # this will save your name and return a message that your name is saved
http://ip_address/names                # this will list all the names you saved in database
```

And thats it, pretty easy huh?
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)